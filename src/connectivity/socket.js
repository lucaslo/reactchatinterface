import io from 'socket.io-client';
import {receiveMessage, updateUser} from '../actions'

let socket 

export function listenForSocketEvents(dispatch) {    
    socket.on('message', (message) => {
        dispatch(receiveMessage(message))
    });

    socket.on('userUpdate', (user) => {
        dispatch(updateUser(user))
    });
}

export function connectSocket(account){
    socket = io.connect('http://localhost:8080', { query: account  });
}

export function sendMessage(message) {
    socket.emit('message', message);
}