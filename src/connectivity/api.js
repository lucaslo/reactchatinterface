function createRequestOptions(params){
    return{
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(params),
        mode:"cors"}
    
} 

export function login(username) {
 return fetch("http://localhost:3000/users/new",createRequestOptions({username:username})).then(res => res.json())
}