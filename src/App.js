import React, { useState, useEffect } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Child from "./components/chat";
import {Grid} from '@material-ui/core';
import Sidebar from "./components/sidebar"
import Login from "./components/login"
import { listenForSocketEvents, connectSocket } from "./connectivity/socket"
import { useSelector, useDispatch } from 'react-redux'


const classes = {
  root: {
    display: "flex",
    flexFlow: "column",
    height: "100vh",
    overflowY: "hidden"
  },
  main: {
    background: "grey",
    flexDirection: "row",
    overflowY: "hidden",
    overflowX: "hidden",
    height: "100%"
  }
};

export default function App(props) {
  const account = useSelector(state => state.account.user)
  let userIsLogged = useSelector(state => state.account.user !== null)

  const [showSideBar, setSideBarVisibility] = useState(false)
  const dispatch = useDispatch()

  const toggleSideBar = () => {
    setSideBarVisibility(!showSideBar)
  }

  useEffect(() => {
    if(userIsLogged){
      connectSocket(account)
      listenForSocketEvents(dispatch)
    }
  
  }, [dispatch, userIsLogged, account]);

  return (
    <div style={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" onClick={toggleSideBar} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6">
            MicroBots Chat
            </Typography>
        </Toolbar>
      </AppBar>

      {userIsLogged ?
        loadChat(showSideBar)
        : <Login></Login>}
    </div>
  );


}

function loadChat(showSideBar) {
  return showSideBar ?
    withSideBar()
    : withoutSideBar()
}

function withSideBar() {
  return <Grid container style={classes.main} >
    <Grid item xs={3} style={classes.main}>
      <Sidebar></Sidebar>
    </Grid>
    <Grid item xs={9} style={classes.main} as>
      <Child />
    </Grid>

  </Grid>
}

function withoutSideBar() {
  return <Child/>
}

