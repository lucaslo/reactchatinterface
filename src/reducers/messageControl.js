const messageControl = (state = {message:"", messageHistory:[], messageIndex:0}, action) => {
    switch (action.type) {
      case "SEND_MESSAGE":
        let newMessageHistory = [...state.messageHistory, action.message]
        return{message:"", messageHistory:newMessageHistory, messageIndex: newMessageHistory.length}
      case "UP_KEY_PRESSED":
        let previousMessageIndex = state.messageIndex-1
        return {...state, messageIndex:previousMessageIndex, message:state.messageHistory[previousMessageIndex]}
      case "DOWN_KEY_PRESSED":
        let nextMessageIndex = state.messageIndex+1
        return {...state, messageIndex:nextMessageIndex, message:state.messageHistory[nextMessageIndex]}
      case "SET_MESSAGE":
        return {...state, message:action.message}
      case "USER_CLICK":
        return {...state, message:state.message + ` @${action.username} `}  
      default:
        return state
    }
  }

  export default messageControl