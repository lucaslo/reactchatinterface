const chatRoom = (state = {messages:[], users:[]}, action) => {
    switch (action.type) {
      case 'RECEIVE_MESSAGE':
        return {...state, messages:[...state.messages, action.message]}
      case 'UPDATE_USER':
        let alreadyInArray = state.users.findIndex(user=>user.username===action.user.username)
        if(alreadyInArray===-1)
          return {...state, users:[...state.users, action.user]}
        else{
          let newUsers = state.users.filter(user=>user.username!==action.user.username)
          return {...state, users:[...newUsers, action.user]}
        }
      case 'DISCONNECT_USER':
        return {...state, users:state.users.filter((user)=>user.username!==action.user.username)}
      case 'SUCCESS_LOGIN':
        return {...state, users:action.payload.users}
      default:
        return state
    }
  }

  export default chatRoom