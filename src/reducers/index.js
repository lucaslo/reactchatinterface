import { combineReducers } from 'redux'
import chatRoom from "./chatRoom"
import account from "./account"
import messageControl from "./messageControl"

export default combineReducers({chatRoom:chatRoom, account: account, messageControl: messageControl})