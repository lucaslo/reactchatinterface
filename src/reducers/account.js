const account = (state = { user: null, expiresIn: null, error: "" }, action) => {
    switch (action.type) {
        case 'SUCCESS_LOGIN':
            return {
                user: action.payload.token.user,
                expiresIn: new Date(action.payload.token.expiresIn)
                , error: ""
            }
        case 'ERROR_LOGIN':
            return {...state, error:action.error}
        case 'REQUEST_LOGIN':
        default:
            return state
    }
}

export default account