import React, { createRef, useEffect, useState } from "react";
import { Box, Paper, Typography } from "@material-ui/core";
import SendMessageBox from "./sendMessageBox"
import { sendMessage } from "../connectivity/socket"
import { useSelector } from 'react-redux'


const chatBox = {
  overflowY: "scroll",
  height: "100%",
  maxHeight: "100%",
  overflowX: "hidden",
  width: "100%",
  justifyContent: "flex-start",
  display: "flex",
  flexDirection: "column"
}
const messageCss = {
  margin: 5,
  padding: 5,
  marginTop: 5,
  maxWidth: "95%",
  minHeight: "fit-content",
  overflowX: "hidden",
  wordWrap: "break-word"
}
const classes = {
  root: {
    background: "grey",
    flexDirection: "row",
    height: '100%',
    display: "flex",
    overflowY: "hidden",
    overflowX: "hidden"
    // height:"calc(100% - 32px)" //height of toolbar if you know it beforehand
  },
  messageFromMainUser: {
    ...messageCss,
    background: "#3f50b5",
    color: "white",
    alignSelf: "flex-end"
  },
  messageFromOtherUsers: {
    ...messageCss,
    alignSelf: "flex-start"
  },
  messageServer: {
    ...messageCss,
    background: "grey",
    textAlign: "center",
    color: "white",
    fontStyle: "italic",
    alignSelf: "center"
  },
  chatBox: {
    ...chatBox,
    justifyContent: "flex-start"

  },
  controlOverflow: {
    overflowX: "hidden",
    overflowY: "hidden"
  }
}

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

export default function Chat(props) {
  const mainUser = useSelector(state => state.account.user)
  const messagesEndRef = createRef()
  const messages = useSelector(state => state.chatRoom.messages)
  const messageStyle = { "mainUser": classes.messageFromMainUser, "otherUsers": classes.messageFromOtherUsers, "server": classes.messageServer }
  const [usersColor, setUsersColor] = useState({})
  const handleSendMessage = (message) => {
    sendMessage(message)
  }
  useEffect(() => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" })
  });

  const getMessageStyle = (message) => {
    if (message.from === mainUser.username) {
      return messageStyle.mainUser
    } else if (message.from === "server") {
      return { ...messageStyle.server, usernameColor: "white" }
    } else {
      if (!usersColor[message.from]) {
        setUsersColor({ ...usersColor, [message.from]: getRandomColor() })
      }
      return { ...messageStyle.otherUsers, usernameColor: usersColor[message.from] }
    }
  }

  return (
    <div style={classes.root} bgcolor="background.paper">
      <Box display="flex" style={classes.controlOverflow} flexDirection="column" p={2} m={2} bgcolor="background.paper" width="100%" >
        <Box flexGrow={10} width="100%" style={classes.chatBox} >
          {messages.map((message) => {
            const messageStyle = getMessageStyle(message)
            return (
              <Paper style={messageStyle} elevation={5} >
                {message.from === "server" ? "" : <Typography style={{ color: messageStyle.usernameColor }} variant="caption" display="block" gutterBottom>
                  {message.from}
                </Typography>}
                {message.text}
              </Paper>
            )
          })}
          <div ref={messagesEndRef} />
        </Box>
        <Box flexGrow={1} display="flex" alignItems="center" >
          <SendMessageBox sendMessage={handleSendMessage}></SendMessageBox>
        </Box>
      </Box>
    </div>
  )


}

