import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { TextField, Paper, Box, Button } from '@material-ui/core';
import { login } from "../connectivity/api"
import { requestLogin, successLogin, failedLogin } from "../actions"
const classes = {
    root: {
        background: "grey",
        height: '100%',
        display: "flex",
        overflowY: "hidden",
        overflowX: "hidden",
        alignItems: "flex-start",
        justifyContent: "center"
        // height:"calc(100% - 32px)" //height of toolbar if you know it beforehand
    },
    Paper: {
        padding: 20,
        margin: 20,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    },
    spacing: {
        margin: 10,
        padding: 5
    }
}

export default function Login() {
    const [username, setUsername] = useState("")
    const dispatch = useDispatch()
    const typedAnotherCharacter = (element) => {
        setUsername(element.target.value)
    }
    const error = useSelector(state => state.account.error)

    const handleLogin = () => {
        dispatch(requestLogin(username))
        setUsername("")
        login(username)
            .then(
                (result) => {
                    if (result.error) {
                        dispatch(failedLogin(result))
                    } else {
                        dispatch(successLogin(result))
                    }
                },
                (error) => {
                    console.log(error)
                    dispatch(failedLogin(error))
                }
            )
    }
    return <Box style={classes.root} m={3}>
        <Paper elevation={3} style={classes.Paper} fontFamily="Monospace">
            You need to chose a username to enter the chat.
         <TextField error={error} id="outlined-basic" onChange={typedAnotherCharacter}
                value={username} label={error? "Try again " : ""} variant="outlined" style={classes.spacing} />

            <Button m={10} variant="contained" color="primary" onClick={handleLogin}>
                Enter
            </Button>
        </Paper>

    </Box>
}