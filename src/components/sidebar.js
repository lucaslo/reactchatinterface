import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PersonIcon from '@material-ui/icons/Person';
import {clickedOnUser} from "../actions"

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: "white",
  },
  nested: {
    paddingLeft: 4,
  },
}));

export default function NestedList() {
  const classes = useStyles();
  const users = useSelector(state => state.chatRoom.users)
  const dispatch = useDispatch()

  const handleUserClick= (username) =>{
    dispatch(clickedOnUser(username))
  }

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <ListSubheader component="div" id="nested-list-subheader">
          Users in this chatRoom
        </ListSubheader>
      }
      className={classes.root}
    >
      {users.filter(user=>user.status!=="offline").map(user => {
        return <ListItem button onClick={()=>handleUserClick(user.username)}>
          <ListItemIcon>
            <PersonIcon />
          </ListItemIcon>
          <ListItemText primary={user.username} />
        </ListItem>
      })}

    </List>
  );
}