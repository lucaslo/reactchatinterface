import React from "react";
import { Box, Paper, TextField, Button } from "@material-ui/core";
import { useSelector, useDispatch } from 'react-redux'
import {upKeyPressed, downKeyPressed, setMessage, sendMessage} from '../actions'

export default function SendMessageBox(props) {
    const message = useSelector(state => state.messageControl.message)
    const messageHistory=  useSelector(state => state.messageControl.messageHistory)
    const messageIndex =  useSelector(state => state.messageControl.messageIndex)
    const dispatch = useDispatch()

    const typedAnotherCharacter = (element) => {
        dispatch(setMessage(element.target.value ))
    }

    const onKeyPress = (element)=>{
       if(element.which === 13) {
          handleSendMessage();
          }
    }
    
    const handleSendMessage = ()=>{
        if(message!==""){
            props.sendMessage(message)
            dispatch(sendMessage(message))   
        }   
    }

     const getHistory= (element)=>{
        if(element.which===38){
            if(messageIndex!==0){
                dispatch(upKeyPressed())
            }
        }else if(element.which===40){
            if(messageIndex!==messageHistory.length-1){
                dispatch(downKeyPressed())
            }
        }
    }

    return <Paper style={{ width: '100%' }} elevation={3} >
            <Box display="flex" p={2}    >
                <TextField
                   id="outlined-full-width"
                   label="Message"
                   style={{ margin: 8 }}
                   fullWidth
                   margin="normal"
                   InputLabelProps={{
                     shrink: true,
                   }}
                   variant="outlined"
                    onChange={typedAnotherCharacter}
                   value={message}
                   onKeyPress={onKeyPress}
                   onKeyDown={getHistory}
                />

                <Button variant="contained" color="primary" style={{ margin: 5 }} onClick={handleSendMessage}>
                    Send
                </Button>
            </Box>

        </Paper>
    
}

