
export const receiveMessage = message => ({
  type: 'RECEIVE_MESSAGE',
  message: message
})

export const sendMessage = message => ({
  type: 'SEND_MESSAGE',
  message
})

export const connectUser = user => ({
  type: 'CONNECT_USER',
  user
})

export const updateUser = user => ({
  type: 'UPDATE_USER',
  user
})
export const disconnectUser = user => ({
  type: 'DISCONNECT_USER',
  user
})

export const requestLogin = username => ({
  type: 'REQUEST_LOGIN',
  username
})

export const successLogin = payload => ({
  type: 'SUCCESS_LOGIN',
  payload
})

export const failedLogin = error => ({
  type: 'ERROR_LOGIN',
  error
})

export const upKeyPressed = () => ({
  type: 'UP_KEY_PRESSED',
})

export const downKeyPressed = () => ({
  type: 'DOWN_KEY_PRESSED',
})

export const setMessage = (message) => ({
  type: 'SET_MESSAGE',
  message
})

export const clickedOnUser = (username) => ({
  type: 'USER_CLICK',
  username
})
